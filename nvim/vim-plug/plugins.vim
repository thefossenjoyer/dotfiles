call plug#begin()

    Plug 'itchyny/lightline.vim'
    Plug 'joshdick/onedark.vim'
    Plug 'KarimElghamry/vim-auto-comment' 
    Plug 'prabirshrestha/vim-lsp'
"    Plug 'mattn/vim-lsp-settings'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'jiangmiao/auto-pairs'
    
    " Theme
    Plug 'nanotech/jellybeans.vim'

    " Elixir
    Plug 'elixir-editors/vim-elixir'
    
    Plug 'dracula/vim', { 'as': 'dracula' }
    Plug 'JLighter/aura.nvim'
    
    " Make custom themes
    Plug 'rktjmp/lush.nvim'    

    " Horizon
    Plug 'ntk148v/vim-horizon'

    " Nord
    Plug 'arcticicestudio/nord-vim' 
    
    " vim-css-color
    Plug 'ap/vim-css-color'
    
    " NERDTree
    Plug 'preservim/nerdtree'

    " Racket
    Plug 'wlangstroth/vim-racket' 

    " Tabs
    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'romgrk/barbar.nvim' 
call plug#end()
